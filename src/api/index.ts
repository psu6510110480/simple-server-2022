import Router from "koa-router"
const router= new Router()

router.get('/api/greet', async(ctx, next) => {
    ctx.body = {msg: 'hello world'}
})

export default router

